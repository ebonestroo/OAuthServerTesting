
Access
curl -X POST --user "gigy:secret" -d "grant_type=password&username=peter@example.com&password=password" http://localhost:8000/gigy/oauth/token

Refreshing
curl -X POST --user "gigy:secret" -d "grant_type=refresh_token&refresh_token=7aed8014-b875-45dc-9256-30ccca724dde" http://localhost:8000/gigy/oauth/token

Getting data
curl -i -H "Accept: application/json" -H "Authorization: Bearer a82d664a-5890-4c29-a50d-cdaab51c07f9" -X GET http://localhost:8000/gigy/people